import { combineReducers } from 'redux'

import profile from './modules/Profile'

export default combineReducers({
  profile,
})

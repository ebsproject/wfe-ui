import axios from '../../utils/axios'

export const initialState = {
  form: [],
}

export const INIT_FORM = 'Form/INIT_FORM'
export const UPDATE_FORM = 'Form/UPDATE_FORM'
export const SUCCESS_UPDATE_FORM = 'Form/SUCCESS_UPDATE_FORM'

export const setDataForm = (payload) => ({
  type: UPDATE_FORM,
  payload,
})

export const updateForm = (data) => async (dispatch) => {
  await axios
    .post('/update-form', {
      post_update_form: {
        data,
      },
    })
    .then((response) => {
      const res = response.data.post_update_form.data[0]
      dispatch(updateForm(res.code))
    })
}

export const successUpdateForm = () => ({
  type: SUCCESS_UPDATE_FORM,
})

export const getInitData = () => async (dispatch) => {
  await axios.get('/profile').then((response) => {
    const { user_data } = response.data.get_profile.data
    dispatch(setDataForm(user_data.data))
  })
  dispatch(successUpdateForm())
}

export default function formReducer(state = initialState, { type, payload }) {
  switch (type) {
    case UPDATE_FORM:
      return {
        ...state,
        form: payload,
      }
    default:
      return state
  }
}

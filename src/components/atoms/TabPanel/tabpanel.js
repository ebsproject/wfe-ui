import React from 'react'
import PropTypes from 'prop-types'

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const TabPanelAtom = React.forwardRef((props, ref) => {
  // Properties of the atom
  const { value, children, index, ...rest } = props

  return (
    /* 
     @prop data-testid: Id to use inside tabpanel.test.js file.
     */
    <div
      data-testid={'TabPanelTestId'}
      role='tabpanel'
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...rest}
      ref={ref}
    >
      {value === index && <div p={3}>{children}</div>}
    </div>
  )
})
// Type and required properties
TabPanelAtom.propTypes = {
  value: PropTypes.number.isRequired,
  index: PropTypes.number.isRequired,
  children: PropTypes.node,
}
// Default properties
TabPanelAtom.defaultProps = {
  children: <p>Hello</p>,
}

export default TabPanelAtom

import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS
import { Button, Snackbar } from '@material-ui/core'
import MuiAlert from '@material-ui/lab/Alert'
// Styles
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}))

function Alert(props) {
  return <MuiAlert elevation={6} variant='filled' {...props} />
}
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const SnackbarAtom = React.forwardRef((props, ref) => {
  // Properties of the atom
  const { color, message, ...rest } = props
  const classes = useStyles()
  const [open, setOpen] = React.useState(true)

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return
    }
    setOpen(false)
  }

  return (
    /* 
     @prop data-testid: Id to use inside snackbar.test.js file.
     */
    <div data-testid={'SnackbarTestId'} className={classes.root} {...rest} ref={ref}>
      <Snackbar
        open={open}
        autoHideDuration={3000}
        onClose={handleClose}
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
      >
        <Alert onClose={handleClose} severity={color}>
          {message}
        </Alert>
      </Snackbar>
    </div>
  )
})
// Type and required properties
SnackbarAtom.propTypes = {
  color: PropTypes.oneOf(['info', 'success', 'warning', 'error']),
  message: PropTypes.string,
}
// Default properties
SnackbarAtom.defaultProps = {
  color: 'success',
  message: 'Successful',
}

export default SnackbarAtom

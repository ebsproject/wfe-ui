/**
 * @render react
 * @name EBS_Button
 */

import React from 'react'
// nodejs library that concatenates classes
import classNames from 'classnames'
// nodejs library to set properties for components
import PropTypes from 'prop-types'

// @material-ui/core components
import Button from '@material-ui/core/Button'
import { makeStyles } from '@material-ui/core/styles'

import styles from './Button.style'

const useStyles = makeStyles(styles)

/**
 * EBS_Button component
 *
 * @param {string} prisma - The color of the button.
 * @param {string} label - The label of the button
 * @param {boolean} disabled - Is button disabled?
 * @param {Function} onClick - onClick callback
    prisma,
    round,
    children,
    simple,
    block,
    link,
    justIcon,
    className,
 */

const EBS_Button = React.forwardRef((props, ref) => {
  const {
    prisma,
    round,
    children,
    simple,
    block,
    link,
    justIcon,
    className,
    ...rest
  } = props
  const classes = useStyles()
  const btnClasses = classNames({
    [classes[prisma]]: prisma,
    [classes.round]: round,
    [classes.simple]: simple,
    [classes.block]: block,
    [classes.link]: link,
    [classes.justIcon]: justIcon,
    [className]: className,
  })
  return (
    <Button className={btnClasses} {...rest} ref={ref}>
      {children}
    </Button>
  )
})

/**
 * Valid props
 */
EBS_Button.propTypes = {
  prisma: PropTypes.oneOf([
    'rose',
    'white',
    'twitter',
    'facebook',
    'google',
    'linkedin',
    'pinterest',
    'youtube',
    'tumblr',
    'github',
    'behance',
    'dribbble',
    'reddit',
    'instagram',
    'transparent',
  ]),
  simple: PropTypes.bool,
  round: PropTypes.bool,
  block: PropTypes.bool,
  link: PropTypes.bool,
  justIcon: PropTypes.bool,
  children: PropTypes.node,
  className: PropTypes.string,
}

/**
 * Default props
 */
// Button.defaultProps = {
//   label: '',
//   type: 'button',
//   disabled: false,
//   onClick: () => {},
// }

export default EBS_Button

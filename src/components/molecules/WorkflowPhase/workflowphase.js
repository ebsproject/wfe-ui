import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import {
  Button,
  IconButton,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Tabs,
  Tab,
  AppBar,
} from '@material-ui/core'
import EbsForm from 'ebs-form'
import Snackbar from 'components/atoms/Snackbar'
import StageList from 'components/molecules/WorkflowStage'
import TabPanel from 'components/atoms/TabPanel'
// Other
import { client } from 'utils/apollo'
import { createWorkflowPhase } from 'utils/gql/workflow/mutations'
import { createHtmlTag } from 'utils/gql/language/mutations'
// Styles
import { makeStyles } from '@material-ui/core/styles'
// Icons
import AddCircleIcon from '@material-ui/icons/AddCircle'
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}))
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const WorkFlowPhaseMolecule = React.forwardRef((props, ref) => {
  const classes = useStyles()
  // Properties of the molecule
  const { workflowId, refresh, workflowName, ...rest } = props
  const [state, setState] = React.useState({
    phaseId: null,
    open: false,
    value: 0,
    alert: null,
    htmltagId: null,
  })
  const definition = {
    name: 'phase',
    title: '',
    items: [
      {
        sm: 12,
        md: 6,
        lg: 6,
        component: 'textField',
        name: 'name',
        label: 'Phase name',
        type: 'text',
      },
      {
        sm: 12,
        md: 6,
        lg: 6,
        component: 'textField',
        name: 'description',
        label: 'Description',
        type: 'text',
      },
      {
        sm: 12,
        md: 6,
        lg: 6,
        component: 'textField',
        name: 'help',
        label: 'Help',
        type: 'text',
      },
      {
        sm: 12,
        md: 6,
        lg: 6,
        component: 'number',
        name: 'sequence',
        label: 'Sequence number',
        type: 'number',
        defaultValue: 1,
      },
    ],
  }
  const handleChange = (event, newValue) => {
    setState({
      ...state,
      value: newValue,
    })
  }
  const handleClose = () => {
    setState({
      ...state,
      phaseId: null,
      alert: null,
      open: false,
      htmltagId: null,
    })
    refresh()
  }
  const handleClickOpen = () => {
    setState({
      ...state,
      open: true,
    })
  }
  const onSubmit = (values) => {
    // insert new htmlTag
    client
      .mutate({
        mutation: createHtmlTag,
        variables: {
          tagName: 'wf_' + workflowId + '.ph_',
        },
      })
      .then((response) => {
        setState({
          ...state,
          htmltagId: response.data.createHtmlTag.id,
        })
        // insert a new phase
        client
          .mutate({
            mutation: createWorkflowPhase,
            variables: {
              ...values.target,
              tenant: 1, // this will be handler from localStorage
              workflowId: workflowId,
              htmltagId: response.data.createHtmlTag.id,
            },
          })
          .then((response) => {
            setState({
              ...state,
              phaseId: response.data.createWorkflowPhase.id,
              alert: <Snackbar message={`New phase added successful`} />,
            })
          })
          .catch((error) => {
            setState({
              ...state,
              alert: (
                <Snackbar
                  message={`Ops!, there are an error: ${error}`}
                  color='error'
                />
              ),
              phaseId: null,
            })
          })
      })
  }
  return (
    /* 
     @prop data-testid: Id to use inside newworkflowphase.test.js file.
     */
    <div data-testid={'WorkFlowPhaseTestId'}>
      <IconButton title='add phase' color='inherit' onClick={handleClickOpen}>
        <AddCircleIcon />
      </IconButton>
      <Dialog
        fullWidth={true}
        maxWidth='md'
        open={state.open}
        onClose={handleClose}
        aria-labelledby='Workflow-Phase'
      >
        <DialogTitle>New Phase</DialogTitle>
        <DialogContent>
          {state.alert}
          <div className={classes.root}>
            <AppBar position='static'>
              <Tabs
                value={state.value}
                indicatorColor='secondary'
                textColor='inherit'
                onChange={handleChange}
              >
                <Tab label='General' />
                <Tab label='Translation' />
              </Tabs>
              <TabPanel value={state.value} index={0}>
                <EbsForm
                  definition={definition}
                  onSubmit={onSubmit}
                  onCancel={handleClose}
                />
                {state.phaseId ? (
                  <StageList phaseId={state.phaseId} workflowId={workflowId} />
                ) : null}
              </TabPanel>
              <TabPanel value={state.value} index={1}>
                <p>Comming soon!</p>
              </TabPanel>
            </AppBar>
          </div>
        </DialogContent>
        {state.phaseId ? (
          <DialogActions>
            <Button onClick={handleClose} variant='outlined' color='primary'>
              Close
            </Button>
          </DialogActions>
        ) : null}
      </Dialog>
    </div>
  )
})
// Type and required properties
WorkFlowPhaseMolecule.propTypes = {}
// Default properties
WorkFlowPhaseMolecule.defaultProps = {}

export default WorkFlowPhaseMolecule

import React from 'react'

import Avatar from '@material-ui/core/Avatar'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles()

function Logo(props) {
  const classes = useStyles()
  return (
    <>
      <Avatar {...props} className={classes.large} />
      <Typography component='h1' variant='h5'>
        {props.children}
      </Typography>
    </>
  )
}

export default Logo

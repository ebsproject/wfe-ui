import React from 'react'
import PropTypes from 'prop-types'
//Core
import {
  IconButton,
  Dialog,
  DialogContent,
  Tabs,
  Tab,
  AppBar,
} from '@material-ui/core'
import EbsForm from 'ebs-form'
//Other Components
import { client } from 'utils/apollo'
import { createWorkflowStage } from 'utils/gql/workflow/mutations'
import { createHtmlTag } from 'utils/gql/language/mutations'
import Snackbar from 'components/atoms/Snackbar'
import TabPanel from 'components/atoms/TabPanel'
// Styles
import { makeStyles } from '@material-ui/core/styles'
//Icons
import AddCircleIcon from '@material-ui/icons/AddCircle'
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}))
export default function ToolbarActions(props) {
  const classes = useStyles()
  const { phaseId, workflowId, selection, refresh, ...rest } = props
  const [state, setState] = React.useState({
    open: false,
    alert: null,
    value: 0,
  })
  const definition = {
    name: 'stage',
    title: '',
    items: [
      {
        sm: 12,
        md: 6,
        lg: 6,
        component: 'textField',
        name: 'name',
        label: 'Stage name',
        type: 'text',
      },
      {
        sm: 12,
        md: 6,
        lg: 6,
        component: 'textField',
        name: 'description',
        label: 'Description',
        type: 'text',
      },
      {
        sm: 12,
        md: 6,
        lg: 6,
        component: 'textField',
        name: 'help',
        label: 'Help',
        type: 'text',
      },
      {
        sm: 12,
        md: 6,
        lg: 6,
        component: 'number',
        name: 'sequence',
        label: 'Sequence number',
        type: 'number',
        defaultValue: 1,
      },
    ],
  }
  const handleChange = (event, newValue) => {
    setState({
      ...state,
      value: newValue,
    })
  }
  const onSubmit = (values) => {
    client
      .mutate({
        mutation: createHtmlTag,
        variables: {
          tagName: 'wf_' + workflowId + '.ph_' + phaseId + '.stg_',
        },
      })
      .then((response) => {
        client
          .mutate({
            mutation: createWorkflowStage,
            variables: {
              ...values.target,
              phaseId: phaseId,
              tenant: 1, // this will be handler from localStorage
              htmltagId: response.data.createHtmlTag.id,
            },
          })
          .then((response) => {
            setState({
              ...state,
              alert: <Snackbar message='Stage added successful' />,
              open: false,
            })
            refresh()
          })
          .catch((error) => {
            console.log(error)
            setState({
              ...state,
              alert: (
                <Snackbar
                  message={`Ops!, there are an error: ${error}`}
                  color='error'
                />
              ),
              open: false,
            })
          })
      })
      .catch((error) => {
        console.log(error)
        setState({
          ...state,
          alert: (
            <Snackbar message={`Ops!, there are an error: ${error}`} color='error' />
          ),
          open: false,
        })
      })
  }
  const handleClickOpen = () => {
    setState({
      ...state,
      open: true,
    })
  }
  const handleClose = () => {
    setState({
      ...state,
      open: false,
    })
  }
  return (
    <React.Fragment>
      {state.alert}
      <IconButton title='add stage' color='inherit' onClick={handleClickOpen}>
        <AddCircleIcon />
      </IconButton>
      <Dialog
        fullWidth={true}
        maxWidth='sm'
        open={state.open}
        onClose={handleClose}
        aria-labelledby='Stage-Phase'
      >
        <DialogContent>
          <div className={classes.root}>
            <AppBar position='static'>
              <Tabs
                value={state.value}
                indicatorColor='secondary'
                textColor='inherit'
                onChange={handleChange}
              >
                <Tab label='General' />
                <Tab label='Translation' />
              </Tabs>
              <TabPanel value={state.value} index={0}>
                <EbsForm
                  definition={definition}
                  onSubmit={onSubmit}
                  onCancel={handleClose}
                />
              </TabPanel>
              <TabPanel value={state.value} index={1}>
                <p>Comming soon!</p>
              </TabPanel>
            </AppBar>
          </div>
        </DialogContent>
      </Dialog>
    </React.Fragment>
  )
}

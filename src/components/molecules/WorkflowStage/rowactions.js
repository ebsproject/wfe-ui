import React from 'react'
import {
  IconButton,
  Button,
  Grid,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Tabs,
  Tab,
  AppBar,
} from '@material-ui/core'
import EbsForm from 'ebs-form'
import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit'
import Snackbar from 'components/atoms/Snackbar'
import { client } from 'utils/apollo'
import {
  deleteWorkflowStage,
  modifyWorkflowStage,
} from 'utils/gql/workflow/mutations'
import TabPanel from 'components/atoms/TabPanel'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}))

export default function RowActions(props) {
  const classes = useStyles()
  const {
    id,
    name,
    description,
    help,
    sequence,
    tenant,
    phaseId,
    workflowId,
    htmltagId,
    refresh,
    showAlert,
    ...rest
  } = props
  const [state, setState] = React.useState({
    open: false,
    confirm: false,
    alert: null,
    value: 0,
  })
  const definition = {
    name: 'stage',
    title: 'Edit Stage',
    items: [
      {
        sm: 12,
        md: 4,
        lg: 4,
        component: 'textField',
        name: 'name',
        label: 'Stage name',
        type: 'text',
        defaultValue: name,
      },
      {
        sm: 12,
        md: 4,
        lg: 4,
        component: 'textField',
        name: 'description',
        label: 'Description',
        type: 'text',
        defaultValue: description,
      },
      {
        sm: 12,
        md: 4,
        lg: 4,
        component: 'textField',
        name: 'help',
        label: 'Help',
        type: 'text',
        defaultValue: help,
      },
      {
        sm: 12,
        md: 4,
        lg: 4,
        component: 'number',
        name: 'sequence',
        label: 'Sequence number',
        type: 'number',
        defaultValue: sequence,
      },
      {
        sm: 12,
        md: 4,
        lg: 4,
        component: 'textField',
        name: 'tenant',
        label: 'Tenant',
        type: 'number',
        defaultValue: tenant,
      },
    ],
  }
  const handleChange = (event, newValue) => {
    setState({
      ...state,
      value: newValue,
    })
  }
  const deleteStage = () => {
    client
      .mutate({
        mutation: deleteWorkflowStage,
        variables: { id: id },
      })
      .then((response) => {
        handleClose()
        refresh()
        showAlert('Item deleted successful')
      })
      .catch((error) => {
        setState({
          ...state,
          alert: (
            <Snackbar message={`Ops!, there are an error: ${error}`} color='error' />
          ),
        })
      })
  }
  const openConfirm = () => {
    setState({
      ...state,
      confirm: true,
    })
  }
  const openEdit = () => {
    setState({
      ...state,
      open: true,
    })
  }
  const handleClose = () => {
    setState({
      ...state,
      confirm: false,
      open: false,
      alert: null,
    })
  }
  const editStage = (values) => {
    client
      .mutate({
        mutation: modifyWorkflowStage,
        variables: { ...values.target, id: id },
      })
      .then((response) => {
        handleClose()
        refresh()
        showAlert('Item updated successful')
      })
      .catch((error) => {
        setState({
          ...state,
          alert: (
            <Snackbar message={`Ops!, there are an error: ${error}`} color='error' />
          ),
        })
      })
  }
  return (
    <div>
      {state.alert}
      <Grid
        container
        direction='row'
        justify='space-between'
        alignItems='center'
        spacing={1}
      >
        <Grid item xs={4} sm={4} md={4}>
          <IconButton title='edit stage' color='primary' onClick={openEdit}>
            <EditIcon />
          </IconButton>
        </Grid>
        <Grid item xs={4} sm={4} md={4}>
          <IconButton title='delete stage' color='secondary' onClick={openConfirm}>
            <DeleteIcon />
          </IconButton>
        </Grid>
      </Grid>
      {/* Delete Dialog */}
      <Dialog open={state.confirm} onClose={handleClose}>
        <DialogTitle>{`Please confirm`}</DialogTitle>
        <DialogContent>
          <DialogContentText>{`Do you want to delete this item?`}</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={deleteStage} color='inherit'>
            Yes
          </Button>
          <Button onClick={handleClose} color='inherit'>
            No
          </Button>
        </DialogActions>
      </Dialog>
      {/* Edit Dialog */}
      <Dialog open={state.open} onClose={handleClose}>
        <DialogContent>
          <div className={classes.root}>
            <AppBar position='static'>
              <Tabs
                value={state.value}
                indicatorColor='secondary'
                textColor='inherit'
                onChange={handleChange}
              >
                <Tab label='General' />
                <Tab label='Translation' />
              </Tabs>
              <TabPanel value={state.value} index={0}>
                <EbsForm
                  definition={definition}
                  onSubmit={editStage}
                  onCancel={handleClose}
                />
                <TabPanel value={state.value} index={1}>
                  <p>Comming soon!</p>
                </TabPanel>
              </TabPanel>
            </AppBar>
          </div>
        </DialogContent>
      </Dialog>
    </div>
  )
}

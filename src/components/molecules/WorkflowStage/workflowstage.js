import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import EbsMasterDetail from 'ebs-grid-lib'
import Snackbar from 'components/atoms/Snackbar'
import ToolbarActions from './toolbaractions'
import RowActions from './rowactions'

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const WorkflowStageMolecule = React.forwardRef((props, ref) => {
  // Properties of the molecule
  const { phaseId, workflowId, ...rest } = props
  const [alert, setAlert] = React.useState(null)
  const columns = [
    { Header: 'Id', accessor: 'id', hidden: true },
    { Header: 'Name', accessor: 'name', filter: true },
    { Header: 'Description', accessor: 'description', filter: true },
    { Header: 'Help', accessor: 'help', filter: true },
    { Header: 'Sequence', accessor: 'sequence', filter: true },
    { Header: 'Tenant', accessor: 'tenant', filter: true },
  ]
  const toolbarActions = (selection, refresh) => {
    return (
      <ToolbarActions
        phaseId={phaseId}
        workflowId={workflowId}
        selection={selection}
        refresh={refresh}
      />
    )
  }

  const showAlert = (message) => {
    setAlert(null)
    setAlert(<Snackbar message={message} />)
  }

  const rowActions = (row, refresh) => {
    return (
      <RowActions
        {...row}
        refresh={refresh}
        showAlert={showAlert}
        workflowId={workflowId}
      />
    )
  }

  return (
    /* 
     @prop data-testid: Id to use inside workflowstage.test.js file.
     */
    <div ref={ref} data-testid={'WorkflowStageTestId'}>
      {alert}
      <EbsMasterDetail
        columns={columns}
        uri={process.env.GRAPHQL_API}
        title='Stages'
        entity='WorkflowStage'
        callstandard='graphql'
        toolbar={true}
        toolbaractions={toolbarActions}
        defaultfilter={[{ mod: 'EQ', col: 'workflowphase.id', val: phaseId }]}
        actions={rowActions}
      />
    </div>
  )
})
// Type and required properties
WorkflowStageMolecule.propTypes = {
  label: PropTypes.String,
  children: PropTypes.node,
}
// Default properties
WorkflowStageMolecule.defaultProps = {
  label: 'Hello world',
  children: null,
}

export default WorkflowStageMolecule

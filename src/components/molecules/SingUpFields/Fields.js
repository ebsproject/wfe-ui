import React, { useState, useEffect } from 'react'
import TextField from '@material-ui/core/TextField'
import Grid from '@material-ui/core/Grid'

export default function SingUpFields({ fname, lname, email, password }) {
  return (
    <Grid container spacing={2}>
      <Grid item xs={12} sm={6}>
        <TextField
          autoComplete='fname'
          name='firstName'
          value={fname}
          variant='outlined'
          required
          fullWidth
          id='firstName'
          label='First Name'
          autoFocus
        />
      </Grid>
      <Grid item xs={12} sm={6}>
        <TextField
          variant='outlined'
          required
          fullWidth
          id='lastName'
          label='Last Name'
          name='lastName'
          value={lname}
          autoComplete='lname'
        />
      </Grid>
      <Grid item xs={12}>
        <TextField
          variant='outlined'
          required
          fullWidth
          id='email'
          label='Email Address'
          name='email'
          value={email}
          autoComplete='email'
        />
      </Grid>
      <Grid item xs={12}>
        <TextField
          variant='outlined'
          required
          fullWidth
          name='password'
          value={password}
          label='Password'
          type='password'
          id='password'
          autoComplete='current-password'
        />
      </Grid>
    </Grid>
  )
}

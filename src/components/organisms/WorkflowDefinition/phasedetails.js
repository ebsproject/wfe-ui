import React from 'react'
import EbsMasterDetail from 'ebs-grid-lib'

export default function StageList(props) {
  const { phaseId, ...rest } = props
  const columns = [
    { Header: 'Id', accessor: 'id', hidden: true },
    { Header: 'Name', accessor: 'name' },
    { Header: 'Description', accessor: 'description' },
    { Header: 'Sequence', accessor: 'sequence' },
    { Header: 'Tenant', accessor: 'tenant' },
  ]

  return (
    <EbsMasterDetail
      columns={columns}
      uri={process.env.GRAPHQL_API}
      entity='WorkflowStage'
      callstandard='graphql'
      toolbar={false}
      defaultfilter={[{ mod: 'EQ', col: 'workflowphase.id', val: phaseId }]}
    />
  )
}

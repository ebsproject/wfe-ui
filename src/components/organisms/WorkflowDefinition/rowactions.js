import React from 'react'
import {
  Grid,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  IconButton,
  Tabs,
  Tab,
  AppBar,
} from '@material-ui/core'
import EbsForm from 'ebs-form'
import Snackbar from 'components/atoms/Snackbar'
import StageList from 'components/molecules/WorkflowStage'
import TabPanel from 'components/atoms/TabPanel'
import { makeStyles } from '@material-ui/core/styles'
import { client } from 'utils/apollo'
import {
  deleteWorkflowPhase,
  modifyWorkflowPhase,
} from 'utils/gql/workflow/mutations'
// Icons
import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}))

export default function(props) {
  const classes = useStyles()
  const { id, name, description, help, sequence, refresh, showAlert } = props
  const [state, setState] = React.useState({
    confirm: false,
    alert: null,
    open: false,
    value: 0,
  })
  const definition = {
    name: 'phase',
    title: 'Edit Phase',
    items: [
      {
        sm: 12,
        md: 4,
        lg: 4,
        component: 'textField',
        name: 'name',
        label: 'Phase name',
        type: 'text',
        defaultValue: name,
      },
      {
        sm: 12,
        md: 4,
        lg: 4,
        component: 'textField',
        name: 'description',
        label: 'Description',
        type: 'text',
        defaultValue: description,
      },
      {
        sm: 12,
        md: 4,
        lg: 4,
        component: 'textField',
        name: 'help',
        label: 'Help',
        type: 'text',
        defaultValue: help,
      },
      {
        sm: 12,
        md: 4,
        lg: 4,
        component: 'textField',
        name: 'sequence',
        label: 'Sequence',
        type: 'number',
        defaultValue: sequence,
      },
    ],
  }
  const handleChange = (event, newValue) => {
    setState({
      ...state,
      value: newValue,
    })
  }
  const openConfirm = () => {
    setState({
      ...state,
      confirm: true,
    })
  }
  const handleClose = () => {
    setState({
      ...state,
      open: false,
      confirm: false,
    })
  }
  const handleOpenEdit = () => {
    setState({
      ...state,
      open: true,
    })
  }
  const handleEdit = (values) => {
    client
      .mutate({
        mutation: modifyWorkflowPhase,
        variables: { ...values.target, id: id },
      })
      .then((response) => {
        handleClose()
        refresh()
        showAlert(`Item updated successful`)
      })
      .catch((error) => {
        setState({
          ...state,
          alert: (
            <Snackbar message={`Ops!, there are an error: ${error}`} color='error' />
          ),
        })
      })
  }
  const deletePhase = () => {
    client
      .mutate({
        mutation: deleteWorkflowPhase,
        variables: { id: id },
      })
      .then((response) => {
        handleClose()
        refresh()
        showAlert('Item deleted successful')
      })
      .catch((error) => {
        setState({
          ...state,
          alert: (
            <Snackbar message={`Ops!, there are an error: ${error}`} color='error' />
          ),
        })
      })
  }
  return (
    <div>
      {state.alert}
      <Grid
        container
        direction='row'
        justify='space-between'
        alignItems='center'
        spacing={1}
      >
        <Grid item xs={4} sm={4} md={4}>
          <IconButton title='edit phase' color='secondary' onClick={handleOpenEdit}>
            <EditIcon />
          </IconButton>
        </Grid>
        <Grid item xs={4} sm={4} md={4}>
          <IconButton title='delete phase' color='secondary' onClick={openConfirm}>
            <DeleteIcon />
          </IconButton>
        </Grid>
      </Grid>
      {/* Delete dialog */}
      <Dialog open={state.confirm} onClose={handleClose}>
        <DialogTitle>{`Please confirm`}</DialogTitle>
        <DialogContent>
          <DialogContentText>{`Do you want to delete this item?`}</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={deletePhase} color='inherit'>
            Yes
          </Button>
          <Button onClick={handleClose} color='inherit'>
            No
          </Button>
        </DialogActions>
      </Dialog>
      {/* Edit dialog */}
      <Dialog
        fullWidth={true}
        maxWidth='md'
        open={state.open}
        onClose={handleClose}
        aria-labelledby='Workflow-Phase'
      >
        <DialogContent>
          <div className={classes.root}>
            <AppBar position='static'>
              <Tabs
                value={state.value}
                indicatorColor='secondary'
                textColor='inherit'
                onChange={handleChange}
              >
                <Tab label='General' />
                <Tab label='Translation' />
              </Tabs>
              <TabPanel value={state.value} index={0}>
                <EbsForm
                  definition={definition}
                  onSubmit={handleEdit}
                  onCancel={handleClose}
                />
                <StageList phaseId={id} />
              </TabPanel>
              <TabPanel value={state.value} index={1}>
                <p>Comming soon!</p>
              </TabPanel>
            </AppBar>
          </div>
        </DialogContent>
        <DialogActions>
          <Button color='primary' variant='outlined' onClick={handleClose}>
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}

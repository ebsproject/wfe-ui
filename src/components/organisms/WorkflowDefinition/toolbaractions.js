import React from 'react'
import PropTypes from 'prop-types'
//Core
import WorkflowPhase from 'components/molecules/WorkflowPhase'

export default function ToolbarActions(props) {
  return (
    <React.Fragment>
      <WorkflowPhase {...props} />
    </React.Fragment>
  )
}

import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND MOLECULES TO USE
import EbsMasterDetail from 'ebs-grid-lib'
import Snackbar from 'components/atoms/Snackbar'
import ToolbarActions from './toolbaractions'
import StageList from './phasedetails'
import RowActions from './rowactions'

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const WorkflowDefinitionOrganism = React.forwardRef((props, ref) => {
  // Properties of the organism
  const { selection, ...rest } = props
  const { id, description } = selection // Workflow_id
  const [alert, setAlert] = React.useState(null)
  const columns = [
    { Header: 'Id', accessor: 'id', hidden: true },
    { Header: 'Name', accessor: 'name', filter: true },
    { Header: 'Description', accessor: 'description', filter: true },
    { Header: 'Help', accessor: 'help', filter: true },
    { Header: 'Sequence', accessor: 'sequence', filter: true },
  ]
  const toolbarActions = (selection, refresh) => {
    return (
      <ToolbarActions
        {...selection}
        refresh={refresh}
        workflowId={id}
        workflowName={description}
      />
    )
  }
  const detail = (row) => {
    return <StageList phaseId={row.original.id} />
  }
  const showAlert = (message) => {
    setAlert(null)
    setAlert(<Snackbar message={message} />)
  }
  const rowActions = (row, refresh) => {
    return <RowActions {...row} refresh={refresh} showAlert={showAlert} />
  }
  return (
    /* 
     @prop data-testid: Id to use inside workflowdefinition.test.js file.
     */
    <div ref={ref} data-testid={'WorkflowDefinitionTestId'}>
      {alert}
      <EbsMasterDetail
        toolbar={true}
        toolbaractions={toolbarActions}
        uri={process.env.GRAPHQL_API}
        entity='WorkflowPhase'
        columns={columns}
        title='Phases'
        callstandard='graphql'
        defaultfilter={[{ mod: 'EQ', col: 'workflow.id', val: id }]}
        detailcomponent={detail}
        actions={rowActions}
      />
    </div>
  )
})
// Type and required properties
WorkflowDefinitionOrganism.propTypes = {}
// Default properties
WorkflowDefinitionOrganism.defaultProps = {}

export default WorkflowDefinitionOrganism

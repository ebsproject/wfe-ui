import React, { useState, useEffect } from 'react'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'
import Grid from '@material-ui/core/Grid'
import { makeStyles } from '@material-ui/core/styles'
import Link from '@material-ui/core/Link'

import FormFields from '../../molecules/SingUpFields'
import { EBS_Button } from '../../atoms/Button'

const useStyles = makeStyles((theme) => ({
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}))

export default function Form({ dataForm }) {
  const classes = useStyles()

  //use the firts element
  const data = dataForm[0]
  return (
    <form className={classes.form} noValidate>
      <Grid container spacing={2}>
        <FormFields
          fname={data.firts_name}
          lname={data.last_name}
          email={data.email}
          password={data.password}
        />
        <Grid item xs={12}>
          <FormControlLabel
            control={<Checkbox value='allowExtraEmails' color='primary' />}
            label='I want to receive inspiration, marketing promotions and updates via email.'
          />
        </Grid>
      </Grid>
      <EBS_Button
        // type='submit'
        onClick={(e) => console.log(data)}
        fullWidth
        variant='contained'
        color='primary'
        // prisma='facebook'
        className={classes.submit}
      >
        Sign Up
      </EBS_Button>
      <Grid container justify='flex-end'>
        <Grid item>
          <Link href='#' variant='body2'>
            Already have an account? Sign in
          </Link>
        </Grid>
      </Grid>
    </form>
  )
}

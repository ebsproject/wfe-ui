import { gql } from '@apollo/client'

export const createHtmlTag = gql`
  mutation createHtmlTag($tagName: String!) {
    createHtmlTag(HtmlTagTo: { id: 0, tagName: $tagName }) {
      id
    }
  }
`

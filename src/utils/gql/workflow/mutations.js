import { gql } from '@apollo/client'
// CREATE
export const createWorkflowPhase = gql`
  mutation createWorkflowPhase(
    $name: String!
    $description: String!
    $help: String!
    $sequence: Int!
    $tenant: Int!
    $workflowId: ID!
    $htmltagId: ID!
  ) {
    createWorkflowPhase(
      WorkflowPhaseTo: {
        name: $name
        description: $description
        help: $help
        sequence: $sequence
        tenant: $tenant
        id: 0
        workflow: { id: $workflowId }
        htmltag: { id: $htmltagId }
      }
    ) {
      id
    }
  }
`
export const createWorkflowStage = gql`
  mutation createWorkflowStage(
    $name: String!
    $description: String!
    $help: String!
    $sequence: Int!
    $tenant: Int!
    $phaseId: ID!
    $htmltagId: ID!
  ) {
    createWorkflowStage(
      WorkflowStageTo: {
        name: $name
        description: $description
        help: $help
        sequence: $sequence
        tenant: $tenant
        id: 0
        workflowphase: { id: $phaseId }
        htmltag: { id: $htmltagId }
      }
    ) {
      id
    }
  }
`
// MODIFY
export const modifyWorkflowPhase = gql`
  mutation modifyWorkflowPhase(
    $id: ID!
    $name: String
    $description: String
    $help: String
    $sequence: Int
  ) {
    modifyWorkflowPhase(
      WorkflowPhaseTo: {
        id: $id
        name: $name
        description: $description
        help: $help
        sequence: $sequence
      }
    ) {
      id
    }
  }
`
export const modifyWorkflowStage = gql`
  mutation modifyWorkflowStage(
    $id: ID!
    $name: String
    $description: String
    $help: String
    $sequence: Int
    $tenant: Int
  ) {
    modifyWorkflowStage(
      WorkflowStageTo: {
        id: $id
        name: $name
        description: $description
        help: $help
        sequence: $sequence
        tenant: $tenant
      }
    ) {
      id
    }
  }
`
// DELETE
export const deleteWorkflowPhase = gql`
  mutation deleteWorkflowPhase($id: Int!) {
    deleteWorkflowPhase(idworkflowphase: $id)
  }
`
export const deleteWorkflowStage = gql`
  mutation deleteWorkflowStage($id: Int!) {
    deleteWorkflowStage(idworkflowstage: $id)
  }
`

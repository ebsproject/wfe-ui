import { gql } from '@apollo/client'

export const findWorkflowPhaseList = gql`
  query findWorkflowPhaseList($workflowId: String!) {
    findWorkflowPhaseList(
      page: { number: 1, size: 100 }
      filters: [{ mod: EQ, col: "workflow.id", val: $workflowId }]
    ) {
      totalElements
      content {
        id
        sequence
      }
    }
  }
`

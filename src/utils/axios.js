import axios from 'axios'

const instance = axios.create({ baseURL: 'http://ebs.module/' })

export default instance

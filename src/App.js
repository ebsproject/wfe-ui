import '@babel/polyfill'
// import { hot } from 'react-hot-loader/root'
import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { StylesProvider, createGenerateClassName } from '@material-ui/styles'
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
// theme
import muiTheme from 'assets/themes/themes.config.json'
// container
import Table from './pages/GridWorkflow'
// mock data for development and test purposes
import { configureStore } from 'reducks'
const store = configureStore()

// mock data for development and test purposes
import './mock'

const generateClassName = createGenerateClassName({
  seed: 'module-name', // change name for your module
})

const theme = createMuiTheme(muiTheme)

class App extends Component {
  render() {
    return (
      <StylesProvider generateClassName={generateClassName}>
        <MuiThemeProvider theme={theme}>
          <Provider store={store}>
            <Router basename='/ebs/workflow'>
              <Switch>
                <Route exact path='/' render={() => <Redirect to='/table' />} />
                <Route path='/table' component={Table} />
              </Switch>
            </Router>
          </Provider>
        </MuiThemeProvider>
      </StylesProvider>
    )
  }

  componentDidCatch(error, info) {
    console.log('App Error:', error)
    console.log('App Error Info:', info)
  }
}

// export default hot(App)
export default App

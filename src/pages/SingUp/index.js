import { connect } from 'react-redux'

import SingUp from './SingUp'
import { getInitData } from 'reducks/modules/Profile'

export default connect(
  (state) => ({
    form: state.profile.form,
  }),
  { getInitData },
)(SingUp)

// export { default as SingUp } from './SingUp'

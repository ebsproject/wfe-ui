import React, { useState, useEffect } from 'react'
import CssBaseline from '@material-ui/core/CssBaseline'
import Box from '@material-ui/core/Box'
import { makeStyles } from '@material-ui/core/styles'
import Container from '@material-ui/core/Container'

import Logo from '../../components/molecules/Logo'
import Copyright from '../../components/molecules/Copyright'
import Form from '../../components/organisms/SingUpForm'

import ImgLogo from '../../assets/images/Logos/EBS.png'

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
}))

export default function SignUp({ getInitData, form }) {
  const classes = useStyles()

  useEffect(() => {
    getInitData()
    // eslint-disable-next-line
  }, [])

  return (
    <Container component='main' maxWidth='xs'>
      <CssBaseline />
      <div className={classes.paper}>
        <Logo alt={'EBS'} src={ImgLogo}>
          Sign Up
        </Logo>
        {form.length ? <Form dataForm={form} /> : <h5>Is loading...</h5>}
      </div>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>
  )
}

import React from 'react'
import PropTypes from 'prop-types'
//Core
import { Container } from '@material-ui/core'
import EbsMasterDetail from 'ebs-grid-lib'
//Other components
import ToolbarActions from './toolbaractions'

export default function GridWorkflowView(props) {
  // Props
  const { ...rest } = props

  const columns = [
    { Header: 'id', accessor: 'id', hidden: true },
    { Header: 'Title', accessor: 'title', filter: true },
    { Header: 'Name', accessor: 'name', filter: true },
    { Header: 'Description', accessor: 'description', filter: true },
    { Header: 'Help', accessor: 'help', filter: true },
  ]

  /* This will be rendered in View
  @prop data-testid: Id to use inside gridworkflow.test.js file.
 */
  return (
    <Container data-testid={'GridWorkflowTestId'} component='main' maxWidth='xl'>
      <EbsMasterDetail
        toolbar={true}
        toolbaractions={ToolbarActions}
        uri={process.env.GRAPHQL_API}
        entity='Workflow'
        columns={columns}
        title='Workflows'
        callstandard='graphql'
      />
    </Container>
  )
}
// Type and required properties
GridWorkflowView.propTypes = {}
// Default properties
GridWorkflowView.defaultProps = {}

import React from 'react'
import PropTypes from 'prop-types'
//Core
import {
  Button,
  IconButton,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from '@material-ui/core'
//Other Components
import WorkflowDefinition from 'components/organisms/WorkflowDefinition'
//Icons
import FormatListNumberedIcon from '@material-ui/icons/FormatListNumbered'

export default function ToolbarActions(selection, refresh) {
  const [open, setOpen] = React.useState(false)
  const handleClickOpen = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }
  return (
    <React.Fragment>
      <IconButton
        title='definition'
        color='inherit'
        onClick={handleClickOpen}
        disabled={selection.length > 0 ? false : true}
      >
        <FormatListNumberedIcon />
      </IconButton>
      <Dialog
        fullWidth={true}
        maxWidth='md'
        open={open}
        onClose={handleClose}
        aria-labelledby='Workflow-Definition'
      >
        <DialogTitle id='Workflow-Definition'>Definition</DialogTitle>
        <DialogContent>
          <WorkflowDefinition
            selection={selection.length > 0 ? selection[0].original : null}
          />
        </DialogContent>
        <DialogActions>
          <Button variant='outlined' onClick={handleClose} color='primary'>
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  )
}

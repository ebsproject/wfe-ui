## Introduction

Microservices get a lot of traction these days. They allow multiple teams to work independently from each other, choose their own technology stacks and establish their own release cycles. Unfortunately, frontend development hasn’t fully capitalized yet on the benefits that microservices offer. The common practice for building websites remains “the monolith”: a single frontend codebase that consumes multiple APIs.

What if we could have microservices on the frontend? This would allow frontend developers to work together with their backend counterparts on the same feature and independently deploy parts of the website — “fragments” such as Header, Product, and Footer. Bringing microservices to the frontend requires a layout service that composes a website out of fragments. This proyect was developed to solve this need.

Under single page application(SPA) we separate our app under modular domains, each of these modules are separate in data layer and view layer, conceptually and structurally. With atomic desing and ducks pattern we solve this.

# Getting Started

## Setup

Make sure you have the following tools:

- Windows or Linux computer.
- Visual Studio Code, Sublime, Atom, VIM, or something similar.
- Web navigator, Chrome or FireFox its ok,
- Nodejs
- Git & Gitflow

> You will need Node.js installed to your computer. We won't get into too much detail about Node.js as it's out of the scope of this documentation. Also you won't need to actually use Node.js, it's only required for the development purposes.

## Installation

To install you need run the command in the project directory.

### `npm install`

Then run

### `npm start`

Runs the app in the development mode.<br>

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.<br>
The page will reload if you make edits.<br>

### `npm test`

Launches the test runner in the interactive watch mode.<br>

### `npm run build`

Builds the app for production to the `build` folder.<br>

Your app is ready to be deployed!

### `npm run serve`

Runs the app in the child mode after you build your proyect.<br>

**Note: you need add a port in `package.json` under scripts **<br>

```
"scripts": {
    "start": "react-scripts start",
    "build": "react-scripts build",
    "serve": "react-scripts serve",
    "test": "react-scripts test",
    "eject": "react-scripts eject"
  },
```

## Project Structure

![Project Structure](./src/assets/img/structure.png 'Project Structure')

config: Internal project configuration such as dockerfile
build: Builded app folder for production usage.
coverage: Test directory configuration.
public: Includes assets, static files that will not be processed by webpack.
src: Main folder
src/assets: Core application styles, themes and images
src/components: Atomic design structure
src/pages: Bussines logic and implementation for components
src/utils: Commonly used functions such as constants and components without views
src/mock: Define the services to simulate with Axios mock adapter
src/reducks: Store for state in application
src/client.entry.js and App.js: Starter point
src/spa.entry.js: Child configuration
.env: Environment configuration
.gitignore: Files to ignore by git
.jsconfig.json: Base URL
junit.xml: Test file report

## Design components

### Google's Material Design

All libraries and custom made components are following Google's Material Design Specications.

### Material-UI

Material-UI is a react ui library that implement Google's Material Design specication

## Configuration

For the configuration options checkout Material UI's theme conguration options.
[ Material UI's theme conguration options](https://material-ui.com/customization/theming/)

Theme congurations are located at src/assets/styles/themesConfig.js

```
{
    "palette": {
        "common": {
            "black": "rgba(0, 0, 0, 1)",
            "white": "#fff"
        },
        "background": {
            "paper": "rgba(255, 255, 255, 1)",
            "default": "rgba(255, 255, 255, 1)"
        },
        "primary": {
            "light": "rgba(119, 188, 31, 1)",
            "main": "rgba(76, 175, 80, 1)",
            "dark": "rgba(67, 139, 0, 1)",
            "contrastText": "#fff"
        },
        "secondary": {
            "light": "rgba(129, 131, 135, 1)",
            "main": "rgba(85, 86, 90, 1)",
            "dark": "rgba(44, 45, 49, 1)",
            "contrastText": "rgba(255, 255, 255, 1)"
        },
        "error": {
            "light": "rgba(255, 89, 89, 1)",
            "main": "rgba(192, 13, 30, 1)",
            "dark": "rgba(106, 17, 16, 1)",
            "contrastText": "#fff"
        },
        "text": {
            "primary": "rgba(0, 0, 0, 0.87)",
            "secondary": "rgba(0, 0, 0, 0.54)",
            "disabled": "rgba(0, 0, 0, 0.38)",
            "hint": "rgba(0, 0, 0, 0.38)"
        }
    }
}
```

### Pattern

In a domain-style structure, content is organized by domain, or "area" of the application it's responsible for. In this case we refer that area by page.

To create a new page just need create a new folder with capital case name under /src/pages file structure.

src/
pages/  
 SingIn/

How to create a Component

Components are categorized by levels, there are three: atoms, molecules and organism.

src/
components/
atoms/
molecules/
organisms/

To create a bussines logic we use redux as store with ducks patter

src/
redux/
modules/

State and bussines logic goes here! (even request services)

# Build and Test

TODO: Describe and show how to build your code and run the tests.

# Contribute

TODO: Explain how other users and developers can contribute to make your code better.

/* eslint-disable */

const path = require('path')
const Dotenv = require('dotenv-webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  // Add hot reloading in development
  entry: path.resolve(__dirname, '../../', 'src/spa.entry.js'), // Start with /spa.entry.js
  output: {
    path: path.resolve(__dirname, '../../', 'dist'),
    filename: 'module.bundle.js',
    libraryTarget: 'system',
  },
  mode: 'production',
  devtool: 'cheap-source-map',
  plugins: [
    new HtmlWebpackPlugin({
      title: 'EBS|Module',
      filename: 'index.html',
      inject: false,
      template: path.resolve(__dirname, '../../', 'public/index.spa.html'),
    }),
    new Dotenv({
      path: path.resolve(__dirname, '../../', '.env.production'),
    }),
  ],
  devServer: {
    contentBase: path.resolve(__dirname, '../../', 'dist'),
  },
}

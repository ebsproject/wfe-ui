/* eslint-disable */
// provides a merge function that concatenates arrays and merges objects creating a new object.
const webpackMerge = require('webpack-merge')
const { PrintAsciiLogo } = require('./AsciiLogo.js')
const commonConfig = require('./webpack.common.js')

PrintAsciiLogo()

const getAddons = (addonsArgs) => {
  const addons = Array.isArray(addonsArgs) ? addonsArgs : [addonsArgs]

  return addons.filter(Boolean).map((name) => require(`./addons/webpack.${name}.js`))
}

module.exports = ({ env, addon }) => {
  const envConfig = require(`./webpack.${env}.js`)

  return webpackMerge(commonConfig, envConfig, ...getAddons(addon))
}

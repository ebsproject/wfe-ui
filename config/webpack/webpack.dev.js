/* eslint-disable */
const HtmlWebpackPlugin = require('html-webpack-plugin')

const path = require('path')
const webpack = require('webpack')
const Dotenv = require('dotenv-webpack')

module.exports = {
  // Add hot reloading in development
  entry: path.resolve(__dirname, '../../', 'src/client.entry.js'), // Start with /client.entry.js
  output: {
    path: path.resolve(__dirname, '../../', 'dist'),
    publicPath: '/',
    filename: 'module.bundle.js',
  },
  mode: 'development',
  devtool: 'eval-source-map',
  plugins: [
    new webpack.HotModuleReplacementPlugin(), // Tell webpack we want hot reloading
    new webpack.NoEmitOnErrorsPlugin(),
    new HtmlWebpackPlugin({
      title: 'EBS|Module',
      filename: 'index.html',
      template: path.resolve(__dirname, '../../', 'public/index.html'),
    }),
    new Dotenv({
      path: path.resolve(__dirname, '../../', '.env.development'),
    }),
  ],
  resolve: {
    extensions: ['*', '.js', '.jsx'],
    alias: {
      'react-dom': '@hot-loader/react-dom',
    },
  },
  devServer: {
    hot: true,
    open: true,
    host: 'localhost',
    port: 8080, // you can change this
    historyApiFallback: true,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': '*',
    },
    stats: {
      children: false, // Hide children information
      maxModules: 0, // Set the maximum number of modules to be shown
    },
    disableHostCheck: true,
  },
}

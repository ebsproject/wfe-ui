/* eslint-disable */
const express = require('express')
const app = express()

// const directory = process.argv[2]
// const port = process.argv[2]
const port = 5000

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', '*')
  next()
})

app.use(function(req, res, next) {
  console.log(req.originalUrl)
  next()
})

app.use(express.static('./dist'))

app.listen(port, () =>
  console.log(`HTTP server listening on port 5000, exposing directory /dist.`),
)
